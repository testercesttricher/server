var dev = false;
var dropbox = require('../routes/dropbox.js');
var google = require('../routes/google.js');
var express = require('express');
var app = express();
var header = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
};
app.use('/dropbox', header, dropbox);
app.use('/google', header, google);
app.get('/', function (req, res) {
    res.sendFile('index.html', { root: __dirname + '/../html/' });
});
var server = app.listen(process.env.PORT || 3000, function () {
    var port = server.address().port;
    console.log("Le serveur tourne actuellement sur : ", port);
});
