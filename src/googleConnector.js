"use strict";
exports.__esModule = true;
var googleConnector = (function () {
    function googleConnector(dev, app_key, app_secret) {
        this.url = require('url');
        this.request = require('request');
        this.scope = "https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.appdata https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.metadata";
        this.dev = dev;
        this.APP_KEY = app_key;
        this.APP_SECRET = app_secret;
    }
    ;
    googleConnector.prototype.generateRedirectURI = function (req) {
        var res = this.url.format({
            protocol: 'https',
            host: req.headers.host,
            pathname: '/google/callback'
        });
        if (this.dev) {
            res = this.url.format({
                protocol: req.protocol,
                host: req.headers.host,
                pathname: '/google/callback'
            });
        }
        return res;
    };
    ;
    googleConnector.prototype.checkTokenExpires = function () {
        var timestamp = new Date().getTime() / 1000;
        if ((timestamp - this.timestamp) > 3590) {
            this.refreshToken();
        }
    };
    ;
    googleConnector.prototype.refreshToken = function () {
        this.request.post('https://www.googleapis.com/oauth2/v4/token', {
            form: {
                grant_type: 'refresh_token',
                refresh_token: this.refresh
            },
            auth: {
                user: this.APP_KEY,
                pass: this.APP_SECRET
            }
        }, function (error, response, body) {
            var data = JSON.parse(body);
            this.token = data.access_token;
            this.token_expires = data.expires_in;
            this.timestamp = new Date().getTime() / 1000;
        });
    };
    ;
    return googleConnector;
}());
exports.googleConnector = googleConnector;
