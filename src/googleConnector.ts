export class googleConnector {

  url = require('url');
  request = require('request');
  dev : boolean;
  APP_KEY : string;
  APP_SECRET : string;
  token : string;
  changement : boolean;
  allocated;
  used;
  refresh : string;
  token_expires : number;
  timestamp : number;
  scope : string = "https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.appdata https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.metadata";
  code : string;

  constructor(dev : boolean, app_key : string, app_secret : string) {
    this.dev = dev;
    this.APP_KEY = app_key;
    this.APP_SECRET = app_secret;
  };


  generateRedirectURI(req) {
    let res = this.url.format({
  		protocol: 'https', //à utiliser sur heroku
  		host: req.headers.host,
  		pathname: '/google/callback'
  	});
    if (this.dev) {
      res = this.url.format({
        protocol: req.protocol, //à utiliser en local
        host: req.headers.host,
        pathname: '/google/callback'
      });
    }
     return res;
  };

  checkTokenExpires() {
    let timestamp = new Date().getTime() / 1000;
    if ((timestamp - this.timestamp) > 3590) {
      this.refreshToken()
    }
  };

  refreshToken() {
    this.request.post('https://www.googleapis.com/oauth2/v4/token', {
      form: {
        grant_type: 'refresh_token',
        refresh_token: this.refresh
      },
      auth: {
        user: this.APP_KEY,
        pass: this.APP_SECRET
      }
    }, function (error, response, body) {
  		var data = JSON.parse(body);

  		// extract bearer token
  		this.token = data.access_token;
      this.token_expires = data.expires_in;
      this.timestamp = new Date().getTime() / 1000;
  	});
  };
}
