export class dropboxConnector {

  crypto = require('crypto');
  url = require('url');
  request = require('request');
  dev : boolean;
  APP_KEY : string;
  APP_SECRET : string;
  token : string;
  allocated;
  used;

  constructor(dev : boolean, app_key : string, app_secret : string) {
    this.dev = dev;
    this.APP_KEY = app_key;
    this.APP_SECRET = app_secret;
  };

  generateCSRFToken() {
    return this.crypto.randomBytes(18).toString('base64').replace(/\//g, '-').replace(/\+/g, '_');
  };


  generateRedirectURI(req) {
    let res = this.url.format({
      protocol: 'https', //à utiliser sur heroku
    	host: req.headers.host,
    	pathname: '/dropbox/callback'
    });
    if (this.dev) {
      res = this.url.format({
        protocol: req.protocol, //à utiliser en local
        host: req.headers.host,
        pathname: '/dropbox/callback'
      });
    }
    return res;
  };

  bytesToSize(bytes) {
    if (bytes == 0) return 0;
    let power = Math.pow(1024, 3);
    let res  = (bytes / power);
    return res.toFixed(3);
  };
}
