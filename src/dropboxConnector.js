"use strict";
exports.__esModule = true;
var dropboxConnector = (function () {
    function dropboxConnector(dev, app_key, app_secret) {
        this.crypto = require('crypto');
        this.url = require('url');
        this.request = require('request');
        this.dev = dev;
        this.APP_KEY = app_key;
        this.APP_SECRET = app_secret;
    }
    ;
    dropboxConnector.prototype.generateCSRFToken = function () {
        return this.crypto.randomBytes(18).toString('base64').replace(/\//g, '-').replace(/\+/g, '_');
    };
    ;
    dropboxConnector.prototype.generateRedirectURI = function (req) {
        var res = this.url.format({
            protocol: 'https',
            host: req.headers.host,
            pathname: '/dropbox/callback'
        });
        if (this.dev) {
            res = this.url.format({
                protocol: req.protocol,
                host: req.headers.host,
                pathname: '/dropbox/callback'
            });
        }
        return res;
    };
    ;
    dropboxConnector.prototype.bytesToSize = function (bytes) {
        if (bytes == 0)
            return 0;
        var power = Math.pow(1024, 3);
        var res = (bytes / power);
        return res.toFixed(3);
    };
    ;
    return dropboxConnector;
}());
exports.dropboxConnector = dropboxConnector;
