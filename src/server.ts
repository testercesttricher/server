let dev : boolean = false;

let dropbox = require('../routes/dropbox.js');
let google = require('../routes/google.js');

let express = require('express');
let app = express();



let header = function (req, res, next) {

    // Website you wish to allow to connect
    res.header('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.header('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
};



/*
 *  Liste des modules à utiliser avec leur route
 */
app.use('/dropbox', header, dropbox);
app.use('/google', header, google);


/*
 *  Ce que renvoi le root du serveur
 */
app.get('/', function (req, res) {
   res.sendFile('index.html', {root: __dirname + '/../html/'});
});


/*
 *  On lance le serveur sur le port "port"
 */

  var server = app.listen(process.env.PORT || 3000, function () {
    var port = server.address().port;
    console.log("Le serveur tourne actuellement sur : ", port);
  });
