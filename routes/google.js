"use strict";
exports.__esModule = true;
var googleConnector_1 = require("../src/googleConnector");
var express = require('express');
var router = express.Router();
var url = require('url');
var request = require('request');
var googleUtil = new googleConnector_1.googleConnector(false, "1010104204055-74c28hdgt8jj7g4gq2lshs0gjn9nchjp.apps.googleusercontent.com", "O8fwhA2Ts1-Tkjfz_QM1W-Gc");
router.get('/', function (req, res) {
    res.redirect(url.format({
        protocol: 'https',
        hostname: 'accounts.google.com',
        pathname: '/o/oauth2/v2/auth',
        query: {
            redirect_uri: googleUtil.generateRedirectURI(req),
            prompt: 'consent',
            response_type: 'code',
            client_id: googleUtil.APP_KEY,
            scope: googleUtil.scope,
            access_type: 'offline'
        }
    }));
});
router.get('/callback', function (req, res) {
    googleUtil.code = req.query.code;
    if (req.query.error) {
        return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
    }
    request.post('https://www.googleapis.com/oauth2/v4/token', {
        form: {
            code: googleUtil.code,
            grant_type: 'authorization_code',
            redirect_uri: googleUtil.generateRedirectURI(req)
        },
        auth: {
            user: googleUtil.APP_KEY,
            pass: googleUtil.APP_SECRET
        }
    }, function (error, response, body) {
        if (req.query.error) {
            return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
        }
        else {
            var data = JSON.parse(body);
            if (data.error) {
                return res.send('ERROR: ' + data.error + " || " + body);
            }
            googleUtil.token = data.access_token;
            googleUtil.refresh = data.refresh_token;
            googleUtil.token_expires = data.expires_in;
            googleUtil.timestamp = new Date().getTime() / 1000;
            console.log(googleUtil.token);
            console.log(googleUtil.refresh);
            console.log(googleUtil.token_expires);
            res.redirect('https://cloud-francois.herokuapp.com/google/success');
        }
    });
});
router.get('/success', function (req, res) {
    res.sendFile('success.html', { root: __dirname + '/../html/' });
});
router.get('/get_files', function (req, res) {
    if (googleUtil.token) {
        googleUtil.checkTokenExpires();
        request.get('https://www.googleapis.com/drive/v2/files?&maxResults=1000&orderBy=folder&prettyPrint=true&includeTeamDriveItems=false', {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                var result = '';
                for (var _i = 0, _a = data.items; _i < _a.length; _i++) {
                    var item = _a[_i];
                    result += "{";
                    result += "kind:\"" + item.mimeType + "\",";
                    result += "id:\"" + item.id + "\",";
                    result += "url:\"" + item.alternateLink + "\",";
                    result += "title:\"" + item.title + "\",";
                    if (item.fileSize) {
                        result += "size:\"" + item.fileSize + "\",";
                    }
                    result += "parent: {";
                    for (var _b = 0, _c = item.parents; _b < _c.length; _b++) {
                        var parent_1 = _c[_b];
                        result += "id:\"" + parent_1.id + "\",";
                        result += "isRoot:\"" + parent_1.isRoot + "\"";
                    }
                    result += "}";
                    result += "owners:\"" + item.ownerNames + "\"";
                    result += "},";
                }
                result.slice(0, -1);
                res.send(result);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/get_used', function (req, res) {
    if (googleUtil.token) {
        request.get('https://www.googleapis.com/drive/v2/about', {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                var usedList = data.quotaBytesByService;
                var result = "0.00";
                for (var _i = 0, usedList_1 = usedList; _i < usedList_1.length; _i++) {
                    var toto = usedList_1[_i];
                    if (toto.serviceName == "DRIVE") {
                        result = toto.bytesUsed;
                    }
                }
                res.send(bytesToSize(result));
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/download/:id', function (req, res) {
    if (googleUtil.token) {
        var id = req.params.id;
        request.get('https://www.googleapis.com/drive/v2/files/' + id, {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                console.log(data);
                res.send(data.alternateLink);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/move/:id/:idParentRM/:idParentADD', function (req, res) {
    if (googleUtil.token) {
        var id = req.params.id;
        var idParentADD = req.params.idParentADD;
        var idParentRM = req.params.idParentRM;
        request.get('https://www.googleapis.com/drive/v2/files/' + id + "&uploadType=media&addParents=" + idParentADD + "&removeParents=" + idParentRM, {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.json(body);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/get_total', function (req, res) {
    if (googleUtil.token) {
        request.get('https://www.googleapis.com/drive/v2/about', {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                res.send(bytesToSize(data.quotaBytesTotal));
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/delete/:id', function (req, res) {
    if (googleUtil.token) {
        var id = req.params.id;
        request["delete"]('https://www.googleapis.com/drive/v2/files/' + id, {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.send("success");
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/disconnect', function (req, res) {
    googleUtil.token = undefined;
    googleUtil.allocated = undefined;
    googleUtil.used = undefined;
    res.send("disconnected");
});
router.put('/create_folder', function (req, res) {
    if (googleUtil.token) {
        var id = req.body.id;
        request.get('https://www.googleapis.com/drive/v2/files/' + id, {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.header("Access-Control-Allow-Origin", "*");
                res.json(body);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/share/:id', function (req, res) {
    if (googleUtil.token) {
        var id = req.params.id;
        request.get('https://www.googleapis.com/drive/v2/files/' + id, {
            'headers': {
                'Authorization': 'Bearer ' + googleUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                res.send(data.alternateLink);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/is_connected', function (req, res) {
    if (googleUtil.token) {
        res.send(true);
    }
    else {
        res.send(false);
    }
});
function bytesToSize(bytes) {
    if (bytes == 0)
        return 0;
    var power = Math.pow(1024, 3);
    var res = (bytes / power);
    return res.toFixed(3);
}
;
module.exports = router;
