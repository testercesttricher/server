"use strict";
exports.__esModule = true;
var dropboxConnector_1 = require("../src/dropboxConnector");
var express = require('express');
var router = express.Router();
var url = require('url');
var request = require('request');
var dropboxUtil = new dropboxConnector_1.dropboxConnector(false, "yl8jno17ob05anw", "b54sgmiuqh03aef");
router.get('/', function (req, res) {
    var csrfToken = dropboxUtil.generateCSRFToken();
    res.cookie('csrf', csrfToken);
    res.redirect(url.format({
        protocol: 'https',
        hostname: 'www.dropbox.com',
        pathname: '/oauth2/authorize',
        query: {
            client_id: dropboxUtil.APP_KEY,
            response_type: 'code',
            state: csrfToken,
            redirect_uri: dropboxUtil.generateRedirectURI(req)
        }
    }));
});
router.get('/callback', function (req, res) {
    if (req.query.error) {
        return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
    }
    request.post('https://api.dropbox.com/oauth2/token', {
        form: {
            code: req.query.code,
            grant_type: 'authorization_code',
            redirect_uri: dropboxUtil.generateRedirectURI(req)
        },
        auth: {
            user: dropboxUtil.APP_KEY,
            pass: dropboxUtil.APP_SECRET
        }
    }, function (error, response, body) {
        if (req.query.error) {
            return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
        }
        else {
            var data = JSON.parse(body);
            dropboxUtil.token = data.access_token;
        }
        res.redirect('https://cloud-francois.herokuapp.com/dropbox/success');
    });
});
router.get('/success', function (req, res) {
    res.sendFile('success.html', { root: __dirname + '/../html/' });
});
router.get('/get_files', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/files/list_folder', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token,
                'Content-Type': 'application/json; charset=utf-8'
            },
            'json': {
                'path': '',
                'recursive': true
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.json(body);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/get_current_account', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/users/get_current_account', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.json(body);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/download/:id', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/files/get_temporary_link', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token,
                'Content-Type': 'application/json; charset=utf-8'
            },
            'json': {
                'path': decodeURI(req.params.id)
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.json(body.link);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.post('/move/:from/:to', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/files/move', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token,
                'Content-Type': 'application/json; charset=utf-8'
            },
            'json': {
                'from_path': decodeURI(req.params.from),
                'to_path': decodeURI(req.params.to),
                'autorename': true
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.send("success");
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/get_used', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/users/get_space_usage', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                res.header("Access-Control-Allow-Origin", "*");
                res.send(dropboxUtil.bytesToSize(data.used));
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/get_total', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/users/get_space_usage', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                var data = JSON.parse(body);
                res.header("Access-Control-Allow-Origin", "*");
                res.send(dropboxUtil.bytesToSize(data.allocation.allocated));
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.post('/delete/:id', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/files/delete', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token,
                'Content-Type': 'application/json; charset=utf-8'
            },
            'json': {
                'path': decodeURI(req.params.id)
            }
        }, function (error, response, body) {
            if (req.query.error) {
                console.log(req.body);
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.send("success");
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/disconnect', function (req, res) {
    dropboxUtil.token = undefined;
    dropboxUtil.allocated = undefined;
    dropboxUtil.used = undefined;
    res.send("disconnected");
});
router.put('/create_folder/:path', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/files/create_folder', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token,
                'Content-Type': 'application/json; charset=utf-8'
            },
            'json': {
                'path': decodeURI(req.params.path),
                'autorename': true
            }
        }, function (error, response, body) {
            if (req.query.error) {
                return res.send('ERROR ' + req.query.error + ': ' + req.query.error_description);
            }
            else {
                res.send("success");
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/share/:path', function (req, res) {
    if (dropboxUtil.token) {
        request.post('https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings', {
            'headers': {
                'Authorization': 'Bearer ' + dropboxUtil.token,
                'Content-Type': 'application/json; charset=utf-8'
            },
            'json': {
                "path": decodeURI(req.params.path)
            }
        }, function (error, response, body) {
            console.log(body.error);
            if (body.error) {
                request.post('https://api.dropboxapi.com/2/sharing/get_shared_links', {
                    'headers': {
                        'Authorization': 'Bearer ' + dropboxUtil.token,
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    'json': {
                        "path": decodeURI(req.params.path)
                    }
                }, function (err, respo, bod) {
                    res.json(bod.links[0].url);
                });
            }
            else {
                res.json(body.url);
            }
        });
    }
    else {
        res.send("You need to be connected");
    }
});
router.get('/is_connected', function (req, res) {
    if (dropboxUtil.token) {
        res.send(true);
    }
    else {
        res.send(false);
    }
});
function bytesToSize(bytes) {
    if (bytes == 0)
        return 0;
    var power = Math.pow(1024, 3);
    var res = (bytes / power);
    return res.toFixed(3);
}
;
module.exports = router;
